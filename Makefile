QUARTO = quarto render --to $@

SRC := src
SOURCES := $(SRC)/sources
EXT := _extensions

WRITTEN_SOURCES := prayercard decorative_plate_perdon
ORAL_SOURCES := tombule bai_carnevâl

.PHONY: all html pdf epub
all html pdf epub: index.qmd $(SRC)/_dict.md $(SOURCES)/_written.md $(SOURCES)/_oral.md
	@chmod +x lualatex
	@echo $(QUARTO)
	@OLD_PATH="$$PATH" PATH=".:$$PATH" $(QUARTO)

index.qmd: README.md
	@echo "Writing $@"
	@echo "# Preface {-}" > $@
	@sed -ne '/<!--README-->/,/<!--\/README-->/p' $< >> $@
	@echo "## License {-}" >> $@
	@sed -ne '/<!--LICENSE-->/,/<!--\/LICENSE-->/p' README.md | sed -E 's/(Created with .*) and(.*)\./\n:::{.content-visible when-format="pdf"}\n\1,\2 and Lua\\LaTeX.\n:::\n\n:::{.content-visible unless-format="pdf"}\n\1 and\2.\n:::/' >> $@

$(SRC)/_dict.md: dict.tsv
	tail -n+2 $< | cut -sf1 | sort -cdfi
	tail -n+2 $< | awk -F'\t' -f dict.awk > $@

$(SOURCES)/_written.md: $(addprefix $(SOURCES)/written/,$(WRITTEN_SOURCES:=.yml))
$(SOURCES)/_oral.md: $(addprefix $(SOURCES)/oral/,$(ORAL_SOURCES:=.yml))
$(SOURCES)/_%.md:
	@echo "Writing $@"
	@for f in $^; do pandoc -L _extensions/sources.lua --id-prefix=$${f##*/%.*} --metadata-file=$$f -f markdown /dev/null -t markdown; echo; done > $@

# $(EXT)/%.lua:
# 	wget https://raw.githubusercontent.com/pandoc/lua-filters/master/$*/$*.lua -O $@

.PHONY: clean
clean:
	git clean -Xdf

