local function it(tab)
  -- if table does not exist return an empty iterator
  if tab == nil then
    return function()end, 0, 0
  end
  return ipairs(tab)
end


function Pandoc(doc)
  local m = doc.meta

  local res = {pandoc.Header(3, m.title)}

  for _, p in it(m.description) do table.insert(res, p) end

  for _, lang in ipairs{m.source, m['koinè'], m.english} do
    if lang ~= nil then
      if lang.title ~= nil then table.insert(res, pandoc.Header(4, lang.title, pandoc.Attr("", {'unnumbered'}))) end
      for _, p in it(lang.content) do
        if pandoc.utils.type(p) == "Inlines" then p = {p} end
        table.insert(res, pandoc.BlockQuote(p))
      end
    end
  end

  for _, p in it(m.comment) do table.insert(res, p) end

  return pandoc.Pandoc(res)
end

