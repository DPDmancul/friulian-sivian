# Dictionary

This is far from a complete dictionary. Here are reported only terms which
differ in the Friulian on Siviàn from standard one, and not even all of them.
For other terms you can refer to [@GDBtf] which translates from Italian
to standard Friulian.

For each entry there is the English word, the one of Friulian of Siviàn (with
pronounce and gender), the standard Friulian one (with genre) and, for a better
translation of certain terms, the Italian word.

{{< include _dict.md >}}

## Other words

Here are collected some terms for which there isn't a direct translation in
English, Italian, and often also in standard Friulian.

- **cràmar**  (/ˈkramar/, M) cramâr: carnical bearer.
- **criùre** (/kriˈure/, F) bitter cold.
- **filzade** o filzide (/filˈt͡sade/, F) a sort of blanket.
- **sgjave** (/ˈzɟave/, F) sgjavine (it. capezzagna): vehicle accessible road between fields.
- **zutu** (/ˈt͡sutu/), contraction of **ze vutu** (/t͡se ˈvutu/), word for word 
  _what do you want_, used to start a sentence about something for which nothing
  can be done.

## Americanisms

During the 50s a lot of people emigrated from Siviàn (and whole Friûl in
general) to find work. The most of them arrived in Canada, and returning home
brought some americanisms. Those words are not commonly used, but sometimes can
be listened talking to a former emigrant.

<!-- TODO wait for pandoc 2.19 to test colspan-->
+-------------+-----------------------+------------+-----+--------------------+------+
| English     | Americanism of Siviàn                    | Friulian of Siviàn        |
+=============+=======================+============+:===:+====================+:====:+
| birthday    | bordei                | /borˈdɛj/  | M   | compleàn           | M    |
+-------------+-----------------------+------------+-----+--------------------+------+
| chewing gum | cingùm                | /t͡ʃinˈgum/ | M   | gome (di mastiâ)   | F    |
+-------------+-----------------------+------------+-----+--------------------+------+
| farm        | farme                 | /ˈfarme/   | F   | cjamps, campagne   | M, F |
+-------------+-----------------------+------------+-----+--------------------+------+
| fence       | fense                 | /ˈfense/   | F   | palâde             | F    |
+-------------+-----------------------+------------+-----+--------------------+------+
| job         | gjobe                 | /ˈɟɔbe/    | F   | lavôr, vore        | M, F |
+-------------+-----------------------+------------+-----+--------------------+------+
| routine     | routìn                | /rowˈtin/  | M   | dran dran          | M    |
+-------------+-----------------------+------------+-----+--------------------+------+
| supermarket | marchete              | /marˈkɛte/ | F   | buteghe            | F    |
+-------------+-----------------------+------------+-----+--------------------+------+

<!--
:::{.list-table aligns=l,l,l,c,l,c}
  * - English
    - []{colspan=3} Americanism of Siviàn
    - []{colspan=2} Friulian of Siviàn
  * - birthday
    - bordei
    - /borˈdɛj/
    - M
    - compleàn
    - M
  * - chewing gum
    - cingùm
    - /t͡ʃinˈgum/
    - M
    - gome (di mastiâ)
    - F
  * - farm
    - farme
    - /ˈfarme/
    -  F
    - cjamps, campagne
    - M, F
  * - fence
    - fense
    - /ˈfense/
    - F
    - palâde
    - F
  * - job
    - gjobe
    - /ˈɟɔbe/
    - F
    - lavôr, vore
    -M, F
  * - supermarket
    - marchete
    - /marˈkɛte/
    - F
    - buteghe
    - F
:::
-->

## Surnames

Here we will see some of the surnames of the people who live in Siviàn and near
villages, along side with their Italian counterpart, which is used in official
documents.

|          |             |          |
| :-       | :-          | :-       |
| Balìn    | /baˈliŋ/    | Meret    |
| Cjazzaìn | /cat͡saˈin/  | Odorico  |
| Comùz    | /koˈmut͡s/   | Comuzzi  |
| Fari     | /ˈfari/     | Odorico  |
| Macôr    | /maˈcoːr/   | Macor    |
| Merêt    | /meˈreːt/   | Meret    |
| ?        | //          | Odorico  |
| Piscjùt  | /pisˈcut/   | Meret    |
| Roc      | /rɔk/       | Rocco    |
| Trevisàn | /treviˈsaŋ/ | Trevisan |
| Tuniz    | /tuˈnit͡s/   | Tonizzo  |
| Viole    | /ˈvjole/    | Viola    |
| Zite     | /t͡site/     | Odorico  |
| Zorat    | /t͡soˈrat/   | Zoratto  |

