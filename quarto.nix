{ stdenv
, lib
, pandoc
, esbuild
, deno
, fetchurl
, nodePackages
, makeWrapper
, writeText
}:
stdenv.mkDerivation rec {
  pname = "quarto";
  version = "1.1.251";
  src = fetchurl {
    url = "https://github.com/quarto-dev/${pname}-cli/releases/download/v${version}/${pname}-${version}-linux-amd64.tar.gz";
    sha256 = "sha256-VEYUEI4xzQPXlyTbCThAW2npBCZNPDJ5x2cWnkNz7RE=remove";
  };

  nativeBuildInputs = [
    makeWrapper
  ];

  patches = [
    (writeText "fix-deno-path.patch" ''
      --- a/bin/quarto
      +++ b/bin/quarto
      @@ -125,4 +125,4 @@ fi
       # Be sure to include any already defined QUARTO_DENO_OPTIONS
       QUARTO_DENO_OPTIONS="--unstable --no-config --cached-only --allow-read --allow-write --allow-run --allow-env --allow-net --allow-ffi ''${QUARTO_DENO_OPTIONS}"

      -"''${QUARTO_DENO}" ''${QUARTO_ACTION} ''${QUARTO_DENO_OPTIONS} ''${QUARTO_DENO_EXTRA_OPTIONS} "''${QUARTO_IMPORT_ARGMAP}" "''${QUARTO_TARGET}" "$@"
      +deno ''${QUARTO_ACTION} ''${QUARTO_DENO_OPTIONS} ''${QUARTO_DENO_EXTRA_OPTIONS} "''${QUARTO_IMPORT_ARGMAP}" "''${QUARTO_TARGET}" "$@"
    '')
  ];

  preFixup = ''
    wrapProgram $out/bin/quarto \
      --prefix PATH : ${lib.makeBinPath [ deno ]} \
      --prefix QUARTO_PANDOC : ${pandoc}/bin/pandoc \
      --prefix QUARTO_ESBUILD : ${esbuild}/bin/esbuild \
      --prefix QUARTO_DART_SASS : ${nodePackages.sass}/bin/sass \
  '';

  installPhase = ''
      runHook preInstall
      mkdir -p $out/bin $out/share
      rm -r bin/tools
      mv bin/* $out/bin
      mv share/* $out/share
      runHook preInstall
  '';

}
