BEGIN{
  COLS = 7;
}
!/^$/ {
  initial = toupper(substr($1,1,1));
  if (initial != initials[letters]) {
    initials[++letters] = initial;
    count[letters] = 0;
  }
  gsub(/ ?, ?/, "/, /", $3);
  sub(/^/, "/", $3); sub(/$$/, "/", $3);
  for (i = 1; i <= NF; ++i) table[letters][count[letters]][i] = $(i);
  ++count[letters];
}
END {
  for (i = 1; i <= letters; ++i) {
    print "## " initials[i] " {-}\n";
    print ":::{.content-visible when-format=\"pdf\"}";
    print "\\bgroup\\small";
    print "\\begin{longtable}{@{}>{\\raggedright\\arraybackslash}m{.16\\textwidth}>{\\raggedright\\arraybackslash}m{.16\\textwidth}>{\\raggedright\\arraybackslash}m{.16\\textwidth}c>{\\raggedright\\arraybackslash}m{.16\\textwidth}c>{\\raggedright\\arraybackslash}m{.16\\textwidth}@{}}";
    print "\\toprule";
    print "English & \\multicolumn{3}{l}{Friulian on Siviàn} & \\multicolumn{2}{l}{Standard} & Italian \\\\"
    print "\\midrule";
    print "\\endhead";
    for (j = 0; j < count[i]; ++j) {
      for (k = 1; k < COLS; ++k)
        printf table[i][j][k] " & ";
      print table[i][j][COLS] " \\\\";
      #if (j != count[i] - 1) print "\\midrule[.5\\lightrulewidth]";
    }
    print "\\bottomrule";
    print "\\end{longtable}";
    print "\\egroup";
    print ":::\n";
    print ":::{.content-visible unless-format=\"pdf\"}";
    print "<table class=\"table\">";
    print "<thead><tr class=\"header\"><th style=\"text-align:left\">English</th><th colspan=\"3\" style=\"text-align:left\">Friulian on Siviàn</th><th colspan=\"2\" style=\"text-align:left\">Standard</th><th style=\"text-align:left\">Italian</th></tr></thead>";
    print "<tbody>";
    for (j = 0; j < count[i]; ++j) {
      printf "<tr>";
      for (k = 1; k <= COLS; ++k) {
        printf "<td style=\"text-align:";
        if (k == 4 || k == 6)
          printf "center";
        else
          printf "left";
        print "\">" table[i][j][k] "</td>";
      }
      print "</tr>";
    }
    print "</tbody>";
    print "</table>";
    print ":::\n";
  }
}
