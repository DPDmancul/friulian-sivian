{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  nativeBuildInputs = with pkgs; let
    quarto = pkgs.callPackage ./quarto.nix {};
  in [
    gnumake
    gnused gawk
    wget cacert
    pandoc
    quarto
    (texlive.combine { inherit (texlive)
      scheme-basic
      koma-script
      xcolor
      booktabs
      etoolbox
      mdwtools
      caption
      float
      tcolorbox
      pgf
      environ
      tikzfill
      pdfcol

      luatexbase
      cm-unicode
      doulossil
      unicode-math
      lualatex-math
      selnolig;
    })
  ];
}
