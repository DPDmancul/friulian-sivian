# The Friulian of Siviàn

See [web version](https://dpdmancul.gitlab.io/friulian-sivian/).

Download [PDF](https://dpdmancul.gitlab.io/friulian-sivian/friulian-sivian.pdf),
[ePub](https://dpdmancul.gitlab.io/friulian-sivian/friulian-sivian.epub).

---

<!--README-->

[Friulian](https://en.wikipedia.org/wiki/Friulian_language) is a
[Rhaeto-Romance language](https://en.wikipedia.org/wiki/Rhaeto-Romance_languages)
spoken mainly in [Friûl](https://en.wikipedia.org/wiki/Friuli) (historical
region of [central Europe](https://en.wikipedia.org/wiki/Central_Europe)).
It has four major dialects and basically each village (_paîs_) has its variant.
In this book we will explore the variant of Friulian spoken in Siviàn (a small
village in southern Friûl, _la basse_), not the Standard Friulian (koinè) ruled 
by law.

This choice was made in order to preserve the local variant of this language.
In facts the local variants are going to disappear in favour of
[Italian language](https://en.wikipedia.org/wiki/Italian_language) and Standard
Friulian. A lot of efforts are made by [ARLeF](https://arlef.it/en/), 
[Societât Filologjiche Furlane](http://www.filologicafriulana.it/), ... to 
preserve the standardized version, but there is almost nothing done to preserve 
the local variants.

The Standard Friulian has the great advantage to preserve the Friulian language 
and to establish a unique way to write and speak in order to be understood by 
all. But it eludes the real Friulian: that spoken by the people everyday, which 
cannot be saved by a standardization.

I have no title to write this book, expect the fact that I was born and live in 
Siviàn. I am not an expert of Friulian, neither of linguistics in some sort.
I started writing this only to do my part in saving this great cultural heritage.
If someone found a way to enhance this work please contact me or leave an issue
on GitLab: every help is welcome and really appreciated.

- GitLab issues: <https://gitlab.com/DPDmancul/friulian-sivian/-/issues>
- E-mail: <davide.peressoni@tuta.io>
- Matrix: `@dpd-:matrix.org`
- Telegram: [`@DPDmancul`](https://t.me/DPDmancul)


<!--/README-->

## Build

1. Clone the repo <https://gitlab.com/DPDmancul/friulian-sivian.git>
2. Optionally run `nix-shell` to get all the required dependencies into 
   the environment
3. Just run `make` to build all;
   `make html`, `make pdf` or `make epub` to build only one format.

## License
<!--LICENSE-->

Version 1.0.0  
(cc) 2012-2022 Davide Peressoni

<center>![Creative Commons License](img/cc-by-sa.png)</center>

This work is licensed under the 
[**Creative Commons Attribution-ShareAlike 
4.0** International License](http://creativecommons.org/licenses/by-sa/4.0/).

**Attribution** -- You must give appropriate credit, provide a link to the
license, and indicate if changes were made. You may do so in any reasonable
manner, but not in any way that suggests the licensor endorses you or your use.

**ShareAlike** -- If you remix, transform, or build upon the material, you must
distribute your contributions under the same license as the original.

_The information contained in this work has been checked and documented with the
greatest possible care.
No liability resulting from its use can be attributed to the Author involved in
its creation, publication and distribution._

Created with [Quarto](https://quarto.org) and [Pandoc](https://pandoc.org/).

The source files from which this book is generated can be found at
<https://gitlab.com/DPDmancul/friulian-sivian>.
<!--/LICENSE-->

