|   Standard  |    Local    |      Etymology      |          Meaning         | Understandable |     ē, ō     |     ė, ŏ     |
|:-----------:|:-----------:|:-------------------:|:------------------------:|:--------------:|:------------:|:------------:|
|     fûc     |   /'fowk/   |       **φώγ**ω      |           fire           |      foug      |    **fōg**   |    **fŏg**   |
| **fog**aron |  /fogo'ron/ |                     |           blaze          |   **fog**oròn  |  **fog**oròn |  **fog**oròn |
|     gnûf    |    /ɲowf/   |      **nov**us      |          new (M)         |      gnouv     |   **gnōv**   |   **gnŏv**   |
|  **gnov**e  |   /'ɲove/   |                     |          new (F)         |    **gnov**e   |   **gnov**e  |   **gnov**e  |
|     cûr     |    /kowr/   |       **cor**       |           heart          |      cour      |    **cōr**   |    **cŏr**   |
|     fûr     |    /fowr/   |      **for**as      |          outside         |      four      |    **fōr**   |    **fŏr**   |
|     sûr     |    /sowr/   |      **sor**or      |          sister          |      sour      |    **sōr**   |    **sŏr**   |
|      ûf     |    /ouf/    |       **ov**um      |            egg           |       ouv      |    **ōv**    |    **ŏv**    |
|   **pôre**  |   /'powre/  |   **p**av**ore**m   |           fear           |      poure     |   **pōre**   |   **pŏre**   |
|      îr     |    /jejr/   |       **her**i      |         yesterday        |      jeir      |    **jēr**   |    **jėr**   |
|  îr l'altri | /jer latri/ |                     | the day before yesterday | **jer** l'atri | **jer**latri | **jer**latri |
|     pît     |    /pejt/   |      **ped**em      |           foot           |      peid      |    **pēd**   |    **pėd**   |
|    mistîr   |  /mi'stejr/ | **mi**ni**ster**ium |        profession        |     misteir    |  **mistēr**  |  **mistėr**  |
|     cîl     |   /tsejl/   |      **cæl**um      |            sky           |      zeil      |    **zēl**   |    **zėl**   |
|     cîi     |   /'tsei/   |                     |          heavens         |     **zei**    |    **zei**   |    **zei**   |
|     mûr     |    /mu:r/   |      **mur**us      |           wall           |     **mûr**    |    **mûr**   |    **mûr**   |
|    al mûr   |  /al mowr/  |       **mor**       |          he dies         |     al mour    |  al **mōr**  |  al **mŏr**  |

